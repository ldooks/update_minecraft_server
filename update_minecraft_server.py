
from distutils.version import StrictVersion
import glob
import json
import os
import re
import requests
import urllib

def check_if_update_needed(latest_version, install_dir):
  """
  check_if_update_needed()

  Checks to see if the local jar exists, or is an older version
  """
  # Check if there is a minecraft_server.jar already in place
  jar_search = glob.glob("%s/minecraft_server.*.jar" % install_dir)

  if jar_search:
    for existing_jar in jar_search:
      current_version = re.findall(
        r"minecraft_server.\s*([\d.]+)", existing_jar
      )[0][:-1]

      # Compare the version numbers
      if StrictVersion(current_version) < StrictVersion(latest_version):
        print "\nThere is an update for minecraft server"
        get_jar(latest_version=get_latest_version(), install_dir=install_dir)

        print "\nRemoving %s\n" % existing_jar
        os.remove(existing_jar)
      else:
        print "\nMinecraft is up to date"
        return
  else:
    print "\nNo minecraft jars found at %s. Installing %s there" % (
      install_dir,
      latest_version
    )
    get_jar(latest_version=get_latest_version(), install_dir=install_dir)

  update_startup_script(
    jar_version=latest_version,
    install_dir=install_dir
  )


def get_latest_version():
  """
  get_latest_version()

  Requests the json file from mojang's site
  """
  # Request the release version
  blob = requests.get(
    'https://launchermeta.mojang.com/mc/game/version_manifest.json'
  ).json()

  # Parse that bullshit for the latest version
  return json.loads(json.dumps(blob))['latest']['release']


def get_jar(latest_version, install_dir):
  """
  get_jar(latest_version, install_dir)

  Downloads the latest_version jar from mojang
  """
  file_url = 'https://s3.amazonaws.com/Minecraft.Download' +\
      "/versions/%s/minecraft_server.%s.jar" % (
        latest_version, latest_version
      )

  server_jar = "%s/minecraft_server.%s.jar" % (install_dir, latest_version)
  urllib.urlretrieve(file_url, server_jar)


def update_startup_script(jar_version, install_dir):
  """
  update_startup_script(jar_vesion, install_dir)

  Writes a bash script in install_dir that will launch minecraft server
  """
  startup_script = "%s/minecraft_server.sh" % install_dir

  content = """#!/bin/sh
BINDIR=$(dirname "$(readlink -fn "$0")")
cd "$BINDIR"
java -Xmx1024M -Xms1024M -jar minecraft_server.%s.jar""" % jar_version

  print "Updating startup script"
  with open(startup_script, 'w') as myfile:
    myfile.write(str(content))
  os.chmod(startup_script, 0755)

if __name__ == '__main__':
  # Update this with the location of your server
  install_dir = '/mnt/2tb/gaming/minecraft'

  check_if_update_needed(
    latest_version=get_latest_version(),
    install_dir=install_dir
  )
